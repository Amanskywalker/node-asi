# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from .models import Question
from django.db.models import Sum

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the voting index.")

def vote(request):
    latest_question_list = Question.objects.order_by('-pub_date')
    context = {
        'latest_question_list': latest_question_list,
    }
    return render(request, 'vote/index.html', context)

def trends(request):
    latest_question_list = Question.objects.order_by('-votes')
    total = Question.objects.aggregate(Sum('votes'))
    context = {
        'latest_question_list': latest_question_list,
        'total_votes': total
    }
    return render(request, 'vote/trends.html', context)
