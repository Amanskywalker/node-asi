#include<bits/stdc++.h>
#include<string.h>
using namespace std;

bool balancedPa(string expression)
{
  stack<char> s;
  char c;

  for (int i=0; i<expression.length(); i++)
  {
    if (expression[i]=='('||expression[i]=='['||expression[i]=='{')
    {
      s.push(expression[i]);
      continue;
    }

    if(expression[i]==')'||expression[i]==']'||expression[i]=='}')
    {
      c = s.top();
      switch (c)
      {
        case '(': if(expression[i] == ')')
                    s.pop();break;
        case '[': if(expression[i] == ']')
                    s.pop();break;
        case '{': if(expression[i] == '}')
                    s.pop();break;
      }
    }
  }

  return (s.empty());
}

int main(int argc, char const *argv[]) {
  string expr = "[a{a(a)}]";
  if (balancedPa(expr))
    cout << "True\n";
  else
    cout << "False\n";
  return 0;
}
